export class StorageUtil {

    private storage: IStorage;

    constructor(storageType: StorageType) {
        switch (storageType) {
            case StorageType.Local:
                this.storage = window.localStorage as IStorage;
                break;
            case StorageType.Session:
                this.storage = window.sessionStorage as IStorage;
                break;
            default:
                this.storage = window.localStorage as IStorage;
                break;
        }
    }

    set<T>(key: string, value: T): void {
        const json: string = JSON.stringify(value);
        const encoded = btoa(json);
        this.storage.setItem(key, encoded);
    }

    get<T>(key: string): T {
        const stored: string = this.storage.getItem(key);
        if (!stored) {
            return null;
        }
        const decoded: string = atob(stored);
        return JSON.parse(decoded) as T;
    }
}

export enum StorageType {
    Local,
    Session
}

export interface IStorage {
    setItem(key: string, value: any): void;

    getItem(key: string): string;

    removeItem(key: string): void;
}
