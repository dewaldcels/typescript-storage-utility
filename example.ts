import { StorageType, StorageUtil } from "./storage.util";

const STORAGE_KEY_USER: string = '_sess:user';

const dewald: User = {
    email: 'topzy@kratz.com',
    fullName: 'Dewald Els'
}

const storage = new StorageUtil(StorageType.Local);
storage.set<User>(STORAGE_KEY_USER, dewald);
const storedUser: User = storage.get<User>(STORAGE_KEY_USER);
console.log(storedUser.fullName);


interface User {
    email: string;
    fullName: string;
}
